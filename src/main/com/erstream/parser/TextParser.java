package com.erstream.parser;

import com.erstream.model.LogInformation;

public class TextParser {

	private LogInformation logInformation = new LogInformation();
	
	private String[] parseWords;
	private String[] parseContent;
	private int leftEventTime;
	private int rigtEventTime;
	private int leftContentBitrate;
	private int rightContentBitrate;
	private int leftContentName;
	private int rigtContentName;
	private int leftContentType;
	private int rightContentType;
	private int leftCacheStatus;
	
	
	public LogInformation parse(String line) throws Exception   {

		
			parseWords = line.replaceAll("\"", "").split(" ");

			leftEventTime = line.indexOf("[");
			rigtEventTime = line.indexOf("]");
			leftContentName = parseWords[6].indexOf("/ID_");
			rigtContentName = parseWords[6].indexOf(".ism/");
			leftCacheStatus = parseWords[15].indexOf("=");

			logInformation.setUserIpAddress(parseWords[0]);
			logInformation.setEventTime(line.substring(leftEventTime + 1, rigtEventTime));
			logInformation.setRequestMethod(parseWords[5]);
			logInformation.setContentAddress(parseWords[6]);
			logInformation.setContentName(parseWords[6].substring(leftContentName + 1, rigtContentName));
	
			parseContent = parseWords[6].split("/");
			leftContentBitrate = parseContent[4].indexOf("(");
			rightContentBitrate = parseContent[4].indexOf(")");
			leftContentType = parseContent[5].indexOf("(");
			rightContentType = parseContent[5].indexOf("=");

			logInformation.setContentBitRate(Integer.valueOf(parseContent[4].substring(leftContentBitrate + 1, rightContentBitrate)));
			logInformation.setContentType(parseContent[5].substring(leftContentType + 1, rightContentType));
			logInformation.setHttpStatus(Integer.valueOf(parseWords[8]));
			logInformation.setTotalSentBytes(Integer.valueOf(parseWords[9]));
			logInformation.setUserAgent(parseWords[11]);
			logInformation.setCacheStatus(parseWords[15].substring(leftCacheStatus + 1));

			
			return logInformation;
	}
}
