package com.erstream.Manager;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;

import com.erstream.data.LogData;
import com.erstream.data.StatisticData;
import com.erstream.logdatabase.LogDatabase;
import com.erstream.model.LogInformation;
import com.erstream.parser.TextParser;


public class AppManager {

	private static final String PATH = "C:\\Users\\Furkan\\Desktop\\";
	private String filePath;
	private LogInformation logInformation = new LogInformation();
	private TextParser textParser = new TextParser();
	private LogData logDatas = new LogData();
	private LogDatabase database = new LogDatabase();

	public AppManager(String fileName) {

		this.filePath = PATH + fileName;

	}
	
	@SuppressWarnings("resource")
	public void start() throws IOException,SQLException {

		
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		String line;

		long startTime = System.currentTimeMillis();

		while ((line = reader.readLine()) != null) {
			
			try {

				logInformation = textParser.parse(line);
				logDatas.put(logInformation);

			} catch (Exception e) {

				continue;
			}
		}
		
		logDatas.setStatisticData();
		database.insert(StatisticData.getObject());
		
		System.out.println("Total Time : " + ((System.currentTimeMillis() - startTime) / 1000));

	}

}
