package com.erstream.logdatabase;


import java.sql.DriverManager;
import java.sql.SQLException;

import com.erstream.data.StatisticData;
import com.erstream.model.LogInformation;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;



public class LogDatabase {
	
	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String CONNECTION = "jdbc:mysql://127.0.0.1:3306/logparse";
	private static final String USER = "root";
	private static final String PASSWORD = "amadeus";

	
	public void insert(StatisticData data) throws SQLException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;

		String insertSql = "insert into statisticdata"
				+ "(uniqueIpCount, theMostWiewingUser, uniqueContentCount, theMostWiewedContent,"
				+ "theMostEnteredBrowser,theMostViewedBitRate,totalSentGigabyte,"
				+ "httpStatus,createdtime) values"
				+ "(?,?,?,?,?,?,?,?,?)";

		try {
			
			connection = getConnection();
			preparedStatement = (PreparedStatement) connection.prepareStatement(insertSql);

			preparedStatement.setInt(1, data.getUniqueIpCount());
			preparedStatement.setString(2, data.getTheMostWiewingUser());
			preparedStatement.setInt(3, data.getUniqueContentCount());
			preparedStatement.setString(4, data.getTheMostWiewedContent());
			preparedStatement.setString(5, data.getTheMostEnteredBrowser());
			preparedStatement.setString(6, data.getTheMostViewedBitRate());
			preparedStatement.setLong(7, data.getTotalSentGigabyte());
			preparedStatement.setString(8, data.getHttpStatus());
			preparedStatement.setTimestamp(9, getCurrentTime());

			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (connection != null) {
				connection.close();
			}

		}

	}

	private static Connection getConnection() {

		Connection connection = null;

		try {

			Class.forName(DRIVER);

		} catch (ClassNotFoundException e) {

				e.printStackTrace();
		}

		try {

			connection = (Connection) DriverManager.getConnection(
					CONNECTION, USER,PASSWORD);
			return connection;

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return connection;

	}

	private static java.sql.Timestamp getCurrentTime() {

		java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime());

	}
	
}
