package com.erstream.data;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import com.erstream.model.LogInformation;

@SuppressWarnings("unused")
public class LogData {

	StatisticData statisticData = StatisticData.getObject();

	private HashMap<String, Integer> ipIDatas = new HashMap<String, Integer>();
	private HashMap<String, Integer> contentNames = new HashMap<String, Integer>();
	private HashMap<String, Integer> userAgents = new HashMap<String, Integer>();
	private HashMap<Integer, Integer> contentbitrates = new HashMap<Integer, Integer>();
	private HashMap<Integer, Integer> httpStatus = new HashMap<Integer, Integer>();

	DecimalFormat decimalFormat = new DecimalFormat("#.##");

	private long totalSentBytes = 0;
	private int defaultvalue = 1;
	private int theBiggestValue = 0;
	private String mostViwed = "";
	private static int GIGABYTE = 1024 * 1024 * 1024;
	private static int DENOMINATOR = 100000;

	private String userIpAdress;
	private String contentName;
	private String userAgent;
	private Integer contentBitrate;
	private Integer status;

	public void put(LogInformation data)
	{

		userIpAdress = data.getUserIpAddress();
		contentName = data.getContentName();
		userAgent = data.getUserAgent();
		contentBitrate = bitRateParse(data.getContentBitRate());
		status = data.getHttpStatus();

		if (ipIDatas.containsKey(userIpAdress))
		{
			ipIDatas.put(userIpAdress, ipIDatas.get(userIpAdress) + 1);

		} else
		{
			ipIDatas.put(userIpAdress, defaultvalue);
		}

		if (contentNames.containsKey(contentName))
		{
			contentNames.put(contentName, contentNames.get(contentName) + 1);

		} else
		{
			contentNames.put(contentName, defaultvalue);
		}

		if (userAgents.containsKey(userAgent))
		{
			userAgents.put(userAgent, userAgents.get(userAgent) + 1);

		} else
		{
			userAgents.put(userAgent, defaultvalue);
		}

		if (contentbitrates.containsKey(contentBitrate))
		{
			contentbitrates.put(contentBitrate, contentbitrates.get(contentBitrate) + 1);
		} else
		{
			contentbitrates.put(contentBitrate, defaultvalue);
		}

		if (httpStatus.containsKey(status))
		{
			httpStatus.put(status, httpStatus.get(status) + 1);
		} else
		{
			httpStatus.put(status, defaultvalue);
		}

		totalSentBytes = totalSentBytes + data.getTotalSentBytes();

	}

	public void setStatisticData()
	{

		statisticData.setUniqueIpCount(ipIDatas.size());
		statisticData.setTheMostWiewingUser(getKeyMaxValue(ipIDatas));
		statisticData.setUniqueContentCount(contentNames.size());
		statisticData.setTheMostWiewedContent(getKeyMaxValue(contentNames));
		statisticData.setTheMostEnteredBrowser(getRatio(userAgents));
		statisticData.setTheMostViewedBitRate(getRatio(contentbitrates));
		statisticData.setHttpStatus(getRatio(httpStatus));
		statisticData.setTotalSentGigabyte(totalSentBytes / GIGABYTE);

	}

	public String getKeyMaxValue(HashMap<String, Integer> map)
	{

		int value;

		for (Entry<String, Integer> entry : map.entrySet())
		{

			value = entry.getValue();

			if (value > theBiggestValue)
			{

				theBiggestValue = value;
				mostViwed = entry.getKey();

			}
		}

		return mostViwed;

	}

	public String getRatio(HashMap<?, Integer> map)
	{

		StringBuilder builder = new StringBuilder();

		double totalValue = 0;
		double resultValue = 0;
		ArrayList<Object> keyList = new ArrayList<Object>();

		for (Entry<?, Integer> entry : map.entrySet())
		{

			totalValue = totalValue + entry.getValue();

			keyList.add(entry.getKey());

		}

		double keyValue;
		for (Object key : keyList)
		{
			keyValue = (double) map.get(key);
			resultValue = (keyValue / totalValue * 100);
			builder.append(key + "-" + decimalFormat.format(resultValue));
			builder.append(" ");

		}

		return builder.toString();

	}

	public int bitRateParse(int value)
	{

		int size = (int) (Math.log10(value) + 1);

		if (size >= 6)
		{
			return ((value / DENOMINATOR) * DENOMINATOR);
		} else
		{
			switch (size)
			{
				case 5:
					value = ((value / 10000) * 1000);
					break;
				case 4:
					value = ((value / 1000) * 100);
					break;
				case 3:
					value = ((value / 100) * 1000);
					break;
				case 2:
					value = ((value / 10) * 10);
					break;

				default:
					break;
			}
		}

		return value;
	}

}
