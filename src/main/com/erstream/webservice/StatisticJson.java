package com.erstream.webservice;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import com.erstream.data.StatisticData;


@WebServlet("/rs")
public class StatisticJson extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public StatisticJson() {
        super();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter writer = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		
		ObjectMapper mapper = new ObjectMapper();
		StatisticData data = StatisticData.getObject();
		String result = mapper.writeValueAsString(data);
		
		writer.print(result);
		writer.flush();
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
