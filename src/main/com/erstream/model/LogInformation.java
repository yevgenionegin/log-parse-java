package com.erstream.model;

public class LogInformation {

	private String userIpAddress;
	private String eventTime;
	private String requestMethod;
	private String contentAddress;
	private String contentName;
	private int contentBitRate;
	private String contentType;
	private int httpStatus;
	private int totalSentBytes;
	private String userAgent;
	private String cacheStatus;

	public String getUserIpAddress() {
		return userIpAddress;
	}

	public void setUserIpAddress(String userIpAddress) {
		this.userIpAddress = userIpAddress;
	}

	public String getEventTime() {
		return eventTime;
	}

	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}

	public String getRequestMethod() {
		return requestMethod;
	}

	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}

	public String getContentAddress() {
		return contentAddress;
	}

	public void setContentAddress(String contentAddress) {
		this.contentAddress = contentAddress;
	}

	public String getContentName() {
		return contentName;
	}

	public void setContentName(String contentName) {
		this.contentName = contentName;
	}

	public int getContentBitRate() {
		return contentBitRate;
	}

	public void setContentBitRate(int contentBitRate) {
		this.contentBitRate = contentBitRate;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	public int getTotalSentBytes() {
		return totalSentBytes;
	}

	public void setTotalSentBytes(int totalSentBytes) {
		this.totalSentBytes = totalSentBytes;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getCacheStatus() {
		return cacheStatus;
	}

	public void setCacheStatus(String cacheStatus) {
		this.cacheStatus = cacheStatus;
	}

}
