package com.erstream.test;

import com.erstream.Manager.AppManager;
import com.erstream.data.StatisticData;

public class AppTest {

	public static void main(String[] arg) {

		AppManager readLogFile = new AppManager("nginx.access.log");

		try {
			
			readLogFile.start();
			StatisticData data = StatisticData.getObject();
			
			System.out.println("\n");
			System.out.println("Unique ip count : " + data.getUniqueIpCount());
			System.out.println("The most wiewing User : " + data.getTheMostWiewingUser());
			System.out.println("Unique content count : " + data.getUniqueContentCount());
			System.out.println("The most wiewing Content : " + data.getTheMostWiewedContent());
			System.out.println("The most wiewing browser : " + data.getTheMostEnteredBrowser());
			System.out.println("Bit Rate : " + data.getTheMostViewedBitRate());
			System.out.println("HTTP Status : " + data.getHttpStatus());
			

			
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

}
